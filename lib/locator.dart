import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mc_gallery/env/env.dart';

import 'features/core/abstracts/router/app_router.dart';
import 'features/core/services/app_lifecycle_service.dart';
import 'features/core/services/connections_service.dart';
import 'features/core/services/local_storage_service.dart';
import 'features/core/services/logging_service.dart';
import 'features/core/services/navigation_service.dart';
import 'features/core/services/overlay_service.dart';
import 'features/home/api/unsplash_images_api.dart';
import 'features/home/services/image_cache_manager_service.dart';
import 'features/home/services/images_service.dart';
import 'features/home/views/gallery/gallery_view_model.dart';
import 'features/home/views/image_carousel/image_carousel_view_model.dart';

GetIt get locate => Locator.instance();

class Locator {
  static GetIt instance() => GetIt.instance;
  static T locate<T extends Object>() => instance().get<T>();

  static Future<void> setup() async {
    final locator = instance();

    _registerAPIs();
    _registerViewModels();

    await _registerServices(locator);
    await _registerRepos(locator);

    _registerSingletons();
  }

  static void _registerAPIs() {
    instance().registerFactory(
      () => UnsplashImagesApi(token: Env.unsplashApiKey),
    );
  }

  static void _registerViewModels() {
    instance().registerFactory(
      () => GalleryViewModel(
        imagesService: ImagesService.locate,
        navigationService: NavigationService.locate,
        imageCacheManagerService: ImageCacheManagerService.locate,
      ),
    );
    instance().registerFactory(
      () => ImageCarouselViewModel(
        imagesService: ImagesService.locate,
      ),
    );
  }

  static FutureOr<void> _registerServices(GetIt it) async {
    it
      ..registerLazySingleton(
        () => NavigationService(
          mcgRouter: McgRouter.locate,
        ),
      )
      ..registerFactory(
        LoggingService.new,
      )
      ..registerSingleton<ConnectionsService>(
        ConnectionsService(
          connectivity: Connectivity(),
          internetConnectionChecker: InternetConnectionChecker(),
          loggingService: LoggingService.locate,
        ),
        signalsReady: true,
        dispose: (final param) async => param.dispose(),
      );
    await it.isReady<ConnectionsService>();

    it
      ..registerLazySingleton(
        () => OverlayService(
          loggingService: LoggingService.locate,
        ),
        dispose: (param) => param.dispose(),
      )
      ..registerSingleton<AppLifecycleService>(
        AppLifecycleService(
          loggingService: LoggingService.locate,
        ),
        dispose: (final param) async => param.dispose(),
      )
      ..registerSingleton<LocalStorageService>(
        LocalStorageService(),
        signalsReady: true,
      );
    await it.isReady<LocalStorageService>();

    it
      ..registerSingleton<ImagesService>(
        ImagesService(
          imagesApi: UnsplashImagesApi.locate,
          localStorageService: LocalStorageService.locate,
          loggingService: LoggingService.locate,
        ),
      )
      ..registerSingleton(
        ImageCacheManagerService(
          appLifecycleService: AppLifecycleService.locate,
          localStorageService: LocalStorageService.locate,
        ),
      );
  }

  static FutureOr<void> _registerRepos(GetIt locator) {}

  static void _registerSingletons() {}
}
