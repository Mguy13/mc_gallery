import 'dart:ui';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import '/features/core/services/app_lifecycle_service.dart';
import '/features/core/services/local_storage_service.dart';
import '/features/core/services/logging_service.dart';
import '/locator.dart';

/// Handles maintaining the caching of downloaded images
class ImageCacheManagerService {
  ImageCacheManagerService(
      {required AppLifecycleService appLifecycleService,
      required LocalStorageService localStorageService})
      : _appLifecycleService = appLifecycleService,
        _localStorageService = localStorageService {
    _init();
  }

  final AppLifecycleService _appLifecycleService;
  final LocalStorageService _localStorageService;
  final LoggingService _loggingService = LoggingService.locate;
  final _cacheManager = DefaultCacheManager();

  void emptyCache() => _cacheManager.emptyCache();

  Future<void> _init() async {
    _appLifecycleService.addListener(
      tag: runtimeType.toString(),
      listener: (final appLifecycleState) async {
        switch (appLifecycleState) {
          case AppLifecycleState.resumed:
            break;
          case AppLifecycleState.inactive:
          case AppLifecycleState.paused:
          case AppLifecycleState.detached:
            _loggingService.info('Discarding cached images');
            await _cacheManager.emptyCache();
            _localStorageService.resetFavourites();
        }
      },
    );
  }

  static ImageCacheManagerService get locate => Locator.locate();
}
