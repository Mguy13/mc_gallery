import 'package:flutter/material.dart';

import '/features/core/data/constants/const_colors.dart';

class CustomWrap extends StatelessWidget {
  const CustomWrap({
    required this.children,
    super.key,
  });

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return children.isNotEmpty
        ? Wrap(
            runSpacing: 24,
            spacing: 8,
            alignment: WrapAlignment.center,
            runAlignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: children,
          )
        : const Icon(
            Icons.image,
            size: 80,
            color: ConstColours.red,
          );
  }
}
