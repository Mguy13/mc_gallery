import '/l10n/generated/l10n.dart';

/// Represents an option for specifying a search strategy, for an [ImageModel]
enum SearchOption {
  local,
  web;

  String get name {
    switch (this) {
      case SearchOption.local:
        return Strings.current.local;
      case SearchOption.web:
        return Strings.current.web;
    }
  }
}
