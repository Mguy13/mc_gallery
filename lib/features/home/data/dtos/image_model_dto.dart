import 'package:json_annotation/json_annotation.dart';

part 'image_model_dto.g.dart';

@JsonSerializable()
class ImageModelDTO {
  const ImageModelDTO({
    required this.uri,
    required this.imageIndex,
    required this.imageName,
  });

  /// An image's target [Uri].
  ///
  /// Storing an image's [ByteData] is more expensive, memory-wise.
  final Uri uri;

  /// A unique identifier that can be used for indexing the image.
  final int imageIndex;

  /// Given name of the image.
  final String imageName;

  factory ImageModelDTO.fromJson(Map<String, dynamic> json) => _$ImageModelDTOFromJson(json);
  Map<String, dynamic> toJson() => _$ImageModelDTOToJson(this);
}
