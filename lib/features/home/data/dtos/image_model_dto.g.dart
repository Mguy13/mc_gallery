// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_model_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageModelDTO _$ImageModelDTOFromJson(Map<String, dynamic> json) =>
    ImageModelDTO(
      uri: Uri.parse(json['uri'] as String),
      imageIndex: json['imageIndex'] as int,
      imageName: json['imageName'] as String,
    );

Map<String, dynamic> _$ImageModelDTOToJson(ImageModelDTO instance) =>
    <String, dynamic>{
      'uri': instance.uri.toString(),
      'imageIndex': instance.imageIndex,
      'imageName': instance.imageName,
    };
