import '../dtos/image_model_dto.dart';

/// Represents an Image, that would be displayed in the gallery.
class ImageModel {
  const ImageModel._({
    required this.uri,
    required this.imageIndex,
    required this.imageName,
    required this.isFavourite,
  });

  factory ImageModel.fromDto({
    required ImageModelDTO imageModelDto,
    required bool isFavourite,
  }) =>
      ImageModel._(
        uri: imageModelDto.uri,
        imageIndex: imageModelDto.imageIndex,
        imageName: imageModelDto.imageName,
        isFavourite: isFavourite,
      );

  /// An image's target [Uri].
  ///
  /// Storing an image's [ByteData] is more expensive, memory-wise.
  final Uri uri;

  /// A unique identifier that can be used for indexing the image.
  final int imageIndex;

  /// Given name of the image.
  final String imageName;

  /// Whether the image was 'Starred' ot not.
  final bool isFavourite;

  ImageModel copyWith({
    Uri? uri,
    int? imageIndex,
    String? imageName,
    bool? isFavourite,
  }) {
    return ImageModel._(
      uri: uri ?? this.uri,
      imageIndex: imageIndex ?? this.imageIndex,
      imageName: imageName ?? this.imageName,
      isFavourite: isFavourite ?? this.isFavourite,
    );
  }
}
