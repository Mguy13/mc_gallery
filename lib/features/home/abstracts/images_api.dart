import 'dart:async';

import 'package:flutter/foundation.dart';

import '/features/core/services/logging_service.dart';
import '../data/dtos/image_model_dto.dart';

/// Interface for implementing image-fetching strategies, specific to a resource location on the internet.
///
/// Since I used a site that was more obscure than the ones in the examples, this (otherwise pointless
/// and convoluting) interface is for adding a bit of flexibility to change strategy to some other site.
abstract class ImagesApi {
  ImagesApi({required this.token});

  /// Access token provided to be used with API calls
  @protected
  final String token;

  /// Returns images fetched through an API as [ImageModelDTO]s.
  FutureOr<Iterable<ImageModelDTO>> fetchImageUri();

  FutureOr<Iterable<ImageModelDTO>> searchImages({
    required String searchStr,
  });

  @protected
  final LoggingService loggingService = LoggingService.locate;
}
