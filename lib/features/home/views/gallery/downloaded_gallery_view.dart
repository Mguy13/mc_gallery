part of 'gallery_view.dart';

class _DownloadedGalleryView extends StatelessWidget {
  const _DownloadedGalleryView({
    required this.galleryViewModel,
    super.key,
  });

  final GalleryViewModel galleryViewModel;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(color: ConstColours.galleryBackgroundColour),
      child: Padding(
        padding: const EdgeInsets.all(8),
        // Using Wrap instead of GridView, to make use of different image sizes
        child: ValueListenableBuilder<bool>(
          valueListenable: galleryViewModel.isViewingFavouriteListenable,
          builder: (context, final isViewingFavourites, _) => !isViewingFavourites
              ? CustomWrap(
                  children: [
                    for (final imageModel in galleryViewModel.imageModels)
                      _StarrableImage(
                        key: ValueKey(imageModel.imageIndex),
                        imageModel: imageModel,
                        galleryViewModel: galleryViewModel,
                      ),
                  ],
                )
              : CustomWrap(
                  children: [
                    for (final favouriteImageModel in galleryViewModel.favouriteImageModels)
                      _StarrableImage(
                        key: ValueKey(favouriteImageModel.imageIndex),
                        imageModel: favouriteImageModel,
                        galleryViewModel: galleryViewModel,
                      ),
                  ],
                ),
        ),
      ),
    );
  }
}

class _StarrableImage extends StatefulWidget {
  const _StarrableImage({
    required this.galleryViewModel,
    required this.imageModel,
    super.key,
  });

  final GalleryViewModel galleryViewModel;
  final ImageModel imageModel;

  @override
  State<_StarrableImage> createState() => _StarrableImageState();
}

class _StarrableImageState extends State<_StarrableImage> {
  late bool isMarkedFavourite;

  @override
  void initState() {
    super.initState();

    isMarkedFavourite = widget.imageModel.isFavourite;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topRight.add(const Alignment(0.2, -0.2)),
      children: [
        GestureDetector(
          onTap: () => widget.galleryViewModel.pushImageCarouselView(
            context,
            imageModel: widget.imageModel,
          ),
          child: Hero(
            tag: widget.imageModel.imageIndex,
            child: CachedNetworkImage(
              imageUrl: widget.imageModel.uri.toString(),
              cacheKey: widget.imageModel.imageIndex.toString(),
              progressIndicatorBuilder: (_, __, final progress) => CircularProgressIndicator(
                value: widget.galleryViewModel.downloadProgressValue(progress: progress),
              ),
            ),
          ),
        ),
        GestureDetector(
          child: isMarkedFavourite
              ? ConstMedia.buildIcon(
                  ConstMedia.favStarFilled,
                  width: 16,
                  height: 16,
                )
              : ConstMedia.buildIcon(
                  ConstMedia.favStarOutline,
                  width: 16,
                  height: 16,
                ),
          onTap: () {
            widget.galleryViewModel.updateImageFavouriteStatus(
              imageModel: widget.imageModel,
              newFavouriteStatus: !isMarkedFavourite,
            );
            setState(() => isMarkedFavourite = !isMarkedFavourite);
          },
        ),
      ],
    );
  }
}
