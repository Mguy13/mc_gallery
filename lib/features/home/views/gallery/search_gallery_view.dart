part of 'gallery_view.dart';

class _SearchGalleryView extends StatelessWidget {
  const _SearchGalleryView({
    required this.galleryViewModel,
    super.key,
  });

  final GalleryViewModel galleryViewModel;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<List<ImageModel>>(
      valueListenable: galleryViewModel.imageSearchResultsListenable,
      builder: (context, final resultsImageModels, _) => FutureBuilder(
        future: galleryViewModel.lastQueryResultDone,
        builder: (context, final snapshot) {
          final Widget displayedWidget;

          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
            case ConnectionState.active:
              displayedWidget = const CircularProgressIndicator();
              break;
            case ConnectionState.done:
              displayedWidget = ValueListenableBuilder<SearchOption>(
                valueListenable: galleryViewModel.searchOptionListenable,
                builder: (context, final searchOption, child) {
                  switch (searchOption) {
                    case SearchOption.local:
                      return CustomWrap(
                        children: [
                          for (final resultsImageModel in resultsImageModels)
                            CachedNetworkImage(
                              imageUrl: resultsImageModel.uri.toString(),
                              cacheKey: resultsImageModel.imageIndex.toString(),
                              progressIndicatorBuilder: (_, __, final progress) =>
                                  CircularProgressIndicator(
                                value: galleryViewModel.downloadProgressValue(progress: progress),
                              ),
                            ),
                        ],
                      );
                    case SearchOption.web:
                      return CustomWrap(
                        children: [
                          for (final imageResult in resultsImageModels)
                            Image.network(
                              imageResult.uri.toString(),
                              loadingBuilder: (context, final child, final loadingProgress) =>
                                  loadingProgress == null
                                      ? child
                                      : Center(
                                          child: CircularProgressIndicator(
                                            value: loadingProgress.expectedTotalBytes != null
                                                ? loadingProgress.cumulativeBytesLoaded /
                                                    loadingProgress.expectedTotalBytes!
                                                : null,
                                          ),
                                        ),
                            ),
                        ],
                      );
                  }
                },
              );
          }

          return AnimatedSwitcher(
            duration: ConstDurations.halfDefaultAnimationDuration,
            child: displayedWidget,
          );
        },
      ),
    );
  }
}
