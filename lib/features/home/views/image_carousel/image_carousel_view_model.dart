import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:mc_gallery/features/home/views/image_carousel/image_carousel_view.dart';

import '/features/core/abstracts/base_view_model.dart';
import '/features/home/services/images_service.dart';
import '/locator.dart';
import '../../data/models/image_model.dart';

class ImageCarouselViewModel extends BaseViewModel {
  ImageCarouselViewModel({
    required ImagesService imagesService,
  }) : _imagesService = imagesService;

  final ImagesService _imagesService;

  late final ValueNotifier<ImageModel> _currentImageModelNotifier;
  ValueListenable<ImageModel> get currentImageModelListenable => _currentImageModelNotifier;

  final CarouselController carouselController = CarouselController();

  @override
  Future<void> initialise(bool Function() mounted, [Object? arguments]) async {
    _currentImageModelNotifier = ValueNotifier(
      _imagesService.imageModels
          .elementAt((arguments! as ImageCarouselViewArguments).imageIndexKey),
    );
    log.info('Initialized with image: ${_currentImageModelNotifier.value.imageIndex}');

    super.initialise(mounted, arguments);
  }

  @override
  Future<void> dispose() async {
    super.dispose();
  }

  void swipedTo({required int newIndex}) {
    _currentImageModelNotifier.value = _imagesService.imageModelAt(index: newIndex);
    log.info('Swiped to image: ${_currentImageModelNotifier.value.imageIndex}');
  }

  void onPreviousPressed() => carouselController.previousPage();

  void onNextPressed() => carouselController.nextPage();

  String get currentImageUrl => currentImageModelListenable.value.uri.toString();
  String get currentImageKey => currentImageModelListenable.value.imageIndex.toString();
  String get currentImageName => currentImageModelListenable.value.imageName;
  int get currentImageIndex => currentImageModelListenable.value.imageIndex;

  int get numberOfImages => _imagesService.numberOfImages;

  double? downloadProgressValue({required DownloadProgress progress}) =>
      progress.totalSize != null ? progress.downloaded / progress.totalSize! : null;

  bool get hasPreviousImage =>
      currentImageModelListenable.value.imageIndex > _imagesService.firstAvailableImageIndex;
  bool get hasNextImage =>
      currentImageModelListenable.value.imageIndex < _imagesService.lastAvailableImageIndex;

  static ImageCarouselViewModel get locate => Locator.locate();
}
