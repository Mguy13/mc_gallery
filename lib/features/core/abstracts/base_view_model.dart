import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

import '/l10n/generated/l10n.dart';
import '../data/enums/view_model_state.dart';
import '../services/logging_service.dart';

abstract class BaseViewModel<T extends Object?> extends ChangeNotifier {
  final ValueNotifier<bool> _isInitialised = ValueNotifier(false);
  ValueListenable<bool> get isInitialised => _isInitialised;

  final ValueNotifier<bool> _isBusy = ValueNotifier(false);
  ValueListenable<bool> get isBusy => _isBusy;

  final ValueNotifier<bool> _hasError = ValueNotifier(false);
  ValueListenable<bool> get hasError => _hasError;
  final ValueNotifier<ViewModelState> _state = ValueNotifier(ViewModelState.isInitialising);
  ValueListenable<ViewModelState> get state => _state;

  final LoggingService log = LoggingService.locate;

  String? _errorMessage;
  String get errorMessage => _errorMessage ?? strings.somethingWentWrong;

  @mustCallSuper
  void initialise(bool Function() mounted, [T? arguments]) {
    _mounted = mounted;
    _isInitialised.value = true;
    _state.value = ViewModelState.isInitialised;
    log.successfulInit(location: runtimeType.toString());
  }

  void setBusy(bool isBusy) {
    _isBusy.value = isBusy;
    if (isBusy) {
      _state.value = ViewModelState.isBusy;
    } else {
      _state.value = ViewModelState.isInitialised;
    }
  }

  void setError(
    bool hasError, {
    String? message,
  }) {
    _errorMessage = hasError ? message : null;
    _hasError.value = hasError;
    if (hasError) {
      _state.value = ViewModelState.hasError;
    } else {
      _state.value = ViewModelState.isInitialised;
    }
  }

  @override
  void dispose() {
    super.dispose();
    log.successfulDispose(location: runtimeType.toString());
  }

  late final bool Function() _mounted;
  void ifMounted(VoidCallback voidCallback) {
    if (_mounted()) {
      voidCallback();
    }
  }

  final Strings strings = Strings.current;

  double width(BuildContext context) => MediaQuery.of(context).size.width;
  double height(BuildContext context) => MediaQuery.of(context).size.height;
}
