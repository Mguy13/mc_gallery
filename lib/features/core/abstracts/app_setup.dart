import 'dart:async';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '/l10n/generated/l10n.dart';
import '/locator.dart';

abstract class AppSetup {
  static Future<void> initialise() async {
    WidgetsFlutterBinding.ensureInitialized();

    await _setupStrings();

    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    await Hive.initFlutter();

    await Locator.setup();
  }

  static final List<Locale> supportedLocales = kReleaseMode
      ? <Locale>[
          const Locale.fromSubtags(languageCode: 'en'),
        ]
      : Strings.delegate.supportedLocales;

  static Locale resolveLocale(List<Locale>? preferredLocales, Iterable<Locale> supportedLocales) {
    for (final locale in preferredLocales ?? const <Locale>[]) {
      // Check if the current device locale is supported
      for (final supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return supportedLocale;
        }
      }
    }
    return supportedLocales.first;
  }

  static Future<void> _setupStrings() async {
    await Strings.load(resolveLocale(WidgetsBinding.instance.window.locales, supportedLocales));
  }

  static void Function(Object error, StackTrace stackTrace) get onUncaughtException => (
        error,
        stackTrace,
      ) {
        log(
          'Error occurred during app startup',
          error: error,
          stackTrace: stackTrace,
        );
      };
}
