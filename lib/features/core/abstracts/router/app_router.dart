import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '/features/home/views/gallery/gallery_view.dart';
import '/features/home/views/image_carousel/image_carousel_view.dart';
import '../../views/error_page_view.dart';
import 'routes.dart';

class McgRouter {
  static final _mcgRouter = McgRouter();
  final router = GoRouter(
    initialLocation: Routes.home.routePath,
    debugLogDiagnostics: true,
    errorPageBuilder: (context, state) => MaterialPage<void>(
      key: state.pageKey,
      child: ErrorPageView(error: state.error),
    ),
    routes: [
      GoRoute(
        path: Routes.home.routePath,
        name: Routes.home.routeName,
        builder: (context, _) => const GalleryView(),
        routes: [
          GoRoute(
            path: Routes.imageCarousel.routePath,
            name: Routes.imageCarousel.routeName,
            builder: (context, state) => ImageCarouselView(
              imageCarouselViewArguments: state.extra! as ImageCarouselViewArguments,
            ),
          ),
        ],
      ),
    ],
  );

  static McgRouter get locate => _mcgRouter;
}
