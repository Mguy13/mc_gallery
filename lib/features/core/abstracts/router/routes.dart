enum Routes {
  home(RoutesInfo(
    routePath: '/',
    routeName: 'Home',
  )),
  imageCarousel(RoutesInfo(
    routePath: 'image_carousel',
    routeName: 'Image Carousel',
  ));

  final RoutesInfo _routeInfo;

  String get routePath => _routeInfo.routePath;
  String get routeName => _routeInfo.routeName;

  const Routes(this._routeInfo);
}

class RoutesInfo {
  const RoutesInfo({
    required this.routePath,
    required this.routeName,
  });

  final String routePath;
  final String routeName;
}
