import 'package:flutter/widgets.dart';

import '/l10n/generated/l10n.dart';
import '../widgets/gap.dart';

class ErrorPageView extends StatelessWidget {
  const ErrorPageView({
    required this.error,
    super.key,
  });

  final Exception? error;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(Strings.current.errorPageMessage),
          Gap.size16,
        ],
      ),
    );
  }
}
