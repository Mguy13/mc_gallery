import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

abstract class ConstMedia {
  static const String favStarFilled = 'assets/icons/star_filled.svg';
  static const String favStarOutline = 'assets/icons/star_outline.svg';

  static SvgPicture buildIcon(
    String iconReference, {
    Color? color,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    Clip clipBehavior = Clip.hardEdge,
    Alignment alignment = Alignment.center,
  }) =>
      SvgPicture.asset(
        iconReference,
        color: color,
        width: width,
        height: height,
        fit: fit,
        clipBehavior: clipBehavior,
        alignment: alignment,
      );
}
