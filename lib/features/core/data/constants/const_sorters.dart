import 'package:string_similarity/string_similarity.dart';

abstract class ConstSorters {
  /// Uses Dice's Coefficient as a similarity metric, for a 2-way comparison, between a [targetWord]
  /// and given words.
  static int stringsSimilarityTarget(String a, String b, {required String targetWord}) =>
      a.similarityTo(targetWord).compareTo(b.similarityTo(targetWord));
}
