import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class ConstColours {
  /// Smoke Gray => a neutral grey with neutral warmth/cold
  static const galleryBackgroundColour = Color.fromRGBO(127, 127, 125, 1);
  static const red = Colors.red;

  static const white = Colors.white;
  static const black = Colors.black;
  static const transparent = Colors.transparent;
}

abstract class ConstThemes {
  static ThemeData get materialLightTheme => ThemeData(
        useMaterial3: true,
        brightness: Brightness.light,
        colorSchemeSeed: ConstColours.white,
      );

  static ThemeData get materialDarkTheme => ThemeData(
        useMaterial3: true,
        brightness: Brightness.dark,
        colorSchemeSeed: ConstColours.white,
      );

  static CupertinoThemeData get cupertinoLightTheme => const CupertinoThemeData(
        brightness: Brightness.light,
      );
  static CupertinoThemeData get cupertinoDarkTheme => const CupertinoThemeData(
        brightness: Brightness.dark,
      );
  static ThemeData get cupertinoThemeLightHack =>
      materialLightTheme.copyWith(cupertinoOverrideTheme: cupertinoLightTheme);
  static ThemeData get cupertinoThemeDarkHack =>
      materialDarkTheme.copyWith(cupertinoOverrideTheme: cupertinoDarkTheme);
}
