import 'package:flutter/widgets.dart';

import 'const_colors.dart';

abstract class ConstText {
  static const _imageOverlayTextStyle = TextStyle(
    color: ConstColours.white,
    fontSize: 28,
  );
  static TextStyle imageOverlayTextStyle(BuildContext context) => _imageOverlayTextStyle;
}
