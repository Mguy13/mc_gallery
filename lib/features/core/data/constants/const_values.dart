abstract class ConstValues {
  static const String httpsScheme = 'https';
  static const String imagesHostServer = 'source.unsplash.com';
  static const List<String> imagesHostUrlPathSegments = ['user', 'c_v_r'];

  static const int numberOfImages = 25;
  static const int minImageSize = 50;
  static const int maxImageSize = 100;

  static const int defaultEmulatedLatencyMillis = 75;
}
