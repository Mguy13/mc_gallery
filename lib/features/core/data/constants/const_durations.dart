abstract class ConstDurations {
  static const Duration tripleDefaultAnimationDuration = Duration(milliseconds: 1200);
  static const Duration doubleDefaultAnimationDuration = Duration(milliseconds: 800);
  static const Duration oneAndHalfDefaultAnimationDuration = Duration(milliseconds: 600);
  static const Duration defaultAnimationDuration = Duration(milliseconds: 400);
  static const Duration halfDefaultAnimationDuration = Duration(milliseconds: 200);
  static const Duration quarterDefaultAnimationDuration = Duration(milliseconds: 100);
  static const Duration zero = Duration.zero;
}
