enum ViewModelState {
  isInitialising,
  isInitialised,
  isBusy,
  hasError;
}
