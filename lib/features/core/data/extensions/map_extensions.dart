import 'dart:collection';

extension MapExtensions<A, B> on Map<A, B> {
  Map<A, B> get deepCopy => {...this};

  /// Returns the values of a [Map] at given [keys] indices.
  Iterable<B> valuesByKeys({required Iterable<A> keys}) => keys.map((final key) => this[key]!);
}

extension LinkedHashMapExtensions<A, B> on LinkedHashMap<A, B> {
  /// Updates the value at [valueIndex] to [newValue], in addition to preserving the order.
  void updateValueAt({
    required int valueIndex,
    required B newValue,
  }) =>
      this[keys.toList()[valueIndex]] = newValue;
}
