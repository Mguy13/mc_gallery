extension IterableExtensions<T> on Iterable<T> {
  Iterable<T> get deepCopy => toList(growable: false);
}
