import 'dart:math';

extension RandomExtensions on Random {
  int nextIntInRange({
    required int min,
    required int max,
  }) =>
      min + nextInt(max + 1 - min);
}
