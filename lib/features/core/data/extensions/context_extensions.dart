import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

extension DarkMode on BuildContext {
  /// is dark mode currently enabled?
  ///
  /// Using the context version in release throws exception.
  bool get isDarkMode =>
      WidgetsBinding.instance.platformDispatcher.platformBrightness == Brightness.dark;

  double get width => MediaQuery.of(this).size.width;
  double get height => MediaQuery.of(this).size.height;
}
