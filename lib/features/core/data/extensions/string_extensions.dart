/// String extensions
extension StringExtensions on String {
  /// Returns true if given word contains atleast all the characters in [targetChars], and `false` otherwise
  ///
  /// Very efficient `O(n)` instead of naive `O(n*m)`
  bool containsAllCharacters({
    required String targetChars,
    bool ignoreCase = true,
  }) {
    final characterSet = ignoreCase
        ? Set<String>.from(targetChars.toLowerCase().split(''))
        : Set<String>.from(targetChars.split(''));
    for (final testChar in ignoreCase ? toLowerCase().split('') : split('')) {
      characterSet.remove(testChar);
      if (characterSet.isEmpty) return true;
    }

    return false;
  }
}
