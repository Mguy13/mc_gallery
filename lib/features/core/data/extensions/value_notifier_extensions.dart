import 'package:flutter/foundation.dart';

extension ValueNotifierBoolExtensions on ValueNotifier<bool> {
  void flipValue() => value = !value;
}
