import 'package:dio/dio.dart';

/// Extensions on [Dio]'s [Response].
extension ResponseExtensions<T> on Response<T> {
  /// Shorthand for getting response's successful state.
  bool get isSuccessful => (data! as Map<String, dynamic>)['response'] as bool;
}
