extension ObjectExtensions on Object? {
  E asType<E>() => this as E;
  E? asNullableType<E>() => this as E?;
}

extension AsCallback<T extends Object> on T {
  T Function() get asCallback => () => this;
}
