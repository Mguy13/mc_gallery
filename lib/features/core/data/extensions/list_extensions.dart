extension ListExtensions<T> on List<T> {
  List<T> get deepCopy => [...this];
}
