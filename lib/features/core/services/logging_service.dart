import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:talker/talker.dart';
import 'package:talker_dio_logger/talker_dio_logger_interceptor.dart';
import 'package:talker_dio_logger/talker_dio_logger_settings.dart';

import '/locator.dart';

/// Handles logging of events.
class LoggingService {
  final Talker _talker = Talker(
    settings: TalkerSettings(
      useHistory: false,
    ),
    logger: TalkerLogger(
      formater: !Platform.isIOS ? const ColoredLoggerFormatter() : const ExtendedLoggerFormatter(),
    ),
    loggerSettings: TalkerLoggerSettings(
      enableColors: !Platform.isIOS,
    ),
    loggerOutput: debugPrint,
  );

  void Function(dynamic msg, [Object? exception, StackTrace? stackTrace]) get fine => _talker.fine;
  void Function(dynamic msg, [Object? exception, StackTrace? stackTrace]) get good => _talker.good;

  void Function(dynamic msg, [Object? exception, StackTrace? stackTrace]) get info =>
      _talker.verbose;
  void Function(dynamic msg, [Object exception, StackTrace stackTrace]) get warning =>
      _talker.warning;

  void Function(dynamic msg, [Object? exception, StackTrace? stackTrace]) get error =>
      _talker.error;
  void Function(Object exception, [StackTrace stackTrace, dynamic msg]) get handle =>
      _talker.handle;
  void Function(Error error, [StackTrace stackTrace, dynamic msg]) get handleError =>
      _talker.handleError;
  void Function(Exception exception, [StackTrace? stackTrace, dynamic msg]) get handleException =>
      _talker.handleException;

  void successfulInit({required String location}) => _talker.good('[$location] I am initialized');
  void successfulDispose({required String location}) => _talker.good('[$location] I am disposed');

  /// Adds logging to dio calls
  void addLoggingInterceptor({required Dio dio}) {
    dio.interceptors.add(
      TalkerDioLogger(
        talker: Talker(
          logger: TalkerLogger(
            formater:
                !Platform.isIOS ? const ColoredLoggerFormatter() : const ExtendedLoggerFormatter(),
          ),
          settings: TalkerSettings(
            useHistory: false,
          ),
          loggerSettings: TalkerLoggerSettings(
            enableColors: !Platform.isIOS,
          ),
        ),
        settings: const TalkerDioLoggerSettings(
          printRequestHeaders: true,
          printResponseHeaders: true,
        ),
      ),
    );
  }

  static LoggingService get locate => Locator.locate();
}
