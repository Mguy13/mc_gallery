import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';

import '/features/home/views/image_carousel/image_carousel_view.dart';
import '/locator.dart';
import '../abstracts/router/app_router.dart';
import '../abstracts/router/routes.dart';

/// Handles the navigation to and fro all the screens in the app.
class NavigationService {
  const NavigationService({
    required McgRouter mcgRouter,
  }) : _mcgRouter = mcgRouter;

  final McgRouter _mcgRouter;

  void pushImageCarouselView(
    BuildContext context, {
    required ImageCarouselViewArguments imageCarouselViewArguments,
  }) =>
      context.pushNamed(
        Routes.imageCarousel.routeName,
        extra: imageCarouselViewArguments,
      );

  static NavigationService get locate => Locator.locate();
}
