import 'package:flutter/widgets.dart';

import '/locator.dart';
import 'logging_service.dart';

class OverlayService {
  OverlayService({
    required LoggingService loggingService,
  }) : _loggingService = loggingService;

  final LoggingService _loggingService;

  final Map<int, OverlayEntry> _overlayEntryMap = {};

  void insertOverlayEntry(
    BuildContext context, {
    required String tag,
    required OverlayEntry overlayEntry,
  }) {
    if (!_overlayEntryMap.containsKey(tag.hashCode) && !overlayEntry.mounted) {
      _overlayEntryMap.addEntries([MapEntry(tag.hashCode, overlayEntry)]);
      try {
        Overlay.of(context, rootOverlay: true)?.insert(overlayEntry);
        //todo(mehul): Fix and not ignore Overlay building while Widget building error.
      } on FlutterError catch (_) {}
      _loggingService.info('Overlay inserted with tag: $tag');
    } else {
      _loggingService.info('Overlay with tag: $tag, NOT inserted');
    }
  }

  void removeOverlayEntry({
    required String tag,
  }) {
    if (_overlayEntryMap.containsKey(tag.hashCode)) {
      final _overlayEntry = _overlayEntryMap[tag.hashCode];
      if (_overlayEntry?.mounted ?? false) {
        _overlayEntryMap[tag.hashCode]?.remove();
        _overlayEntryMap.remove(tag.hashCode);
        _loggingService.info('Overlay removed with tag: $tag');
      } else {
        _loggingService.info('Overlay with tag: $tag already mounted OR not found. Skipped');
      }
    } else {
      _loggingService.info('Overlay with tag: $tag already exists. Skipped');
    }
  }

  void dispose() {
    for (final overlayEntry in _overlayEntryMap.values) {
      if (overlayEntry.mounted) {
        overlayEntry.remove();
      }
    }
    _overlayEntryMap.clear();
  }

  static OverlayService get locate => Locator.locate();
}
