import 'package:hive/hive.dart';

import '/locator.dart';
import 'logging_service.dart';

/// Handles storing state data locally, onto the device itself
class LocalStorageService {
  LocalStorageService() {
    _init();
  }

  final LoggingService _loggingService = LoggingService.locate;

  late final Box<bool> _userBox;

  static const String _userBoxKey = 'userBoxKey';

  Future<void> _init() async {
    _userBox = await Hive.openBox(_userBoxKey);

    Locator.instance().signalReady(this);
  }

  Iterable<bool> get storedFavouritesStates => _userBox.values;

  void initNewFavourites({required Iterable<bool> newValues}) {
    _userBox.addAll(newValues);
    _loggingService.info('Adding new favourites value');
  }

  void updateFavourite({
    required int index,
    required bool newValue,
  }) {
    try {
      _userBox.putAt(index, newValue);
      _loggingService.good('Successfully updated favourite status at $index -> $newValue');
    } on Exception catch (ex, stackTrace) {
      _loggingService.handleException(ex, stackTrace);
    }
  }

  void resetFavourites() {
    _userBox.clear();
    _loggingService.info('Cleared favourites table');
  }

  static LocalStorageService get locate => Locator.locate();
}
