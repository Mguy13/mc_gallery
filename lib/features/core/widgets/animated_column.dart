import 'package:flutter/widgets.dart';

import '/features/core/data/constants/const_durations.dart';

/// [AnimatedColumn] Animates its children when they get added or removed at the end
class AnimatedColumn extends StatelessWidget {
  const AnimatedColumn({
    required this.children,
    this.duration = ConstDurations.oneAndHalfDefaultAnimationDuration,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.mainAxisSize = MainAxisSize.max,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.textDirection,
    this.verticalDirection = VerticalDirection.down,
    this.textBaseline,
    this.maxAnimatingChildren = 2,
    super.key,
  });

  /// [duration] specifies the duration of the add/remove animation
  final Duration duration;

  /// [maxAnimatingChildren] determines the maximum number of chidren that can
  /// be animating at once, if more are removed or added at within an animation
  /// duration they will pop in instead
  final int maxAnimatingChildren;

  final MainAxisAlignment mainAxisAlignment;
  final MainAxisSize mainAxisSize;
  final CrossAxisAlignment crossAxisAlignment;
  final TextDirection? textDirection;
  final VerticalDirection verticalDirection;
  final TextBaseline? textBaseline;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: [
        for (int i = 0; i < children.length + maxAnimatingChildren; i++)
          AnimatedSwitcher(
            duration: duration,
            switchInCurve: Curves.easeInOut,
            switchOutCurve: Curves.easeInOut,
            transitionBuilder: (child, animation) => FadeTransition(
              opacity: animation,
              child: SizeTransition(
                sizeFactor: animation,
                axisAlignment: -1,
                child: child,
              ),
            ),
            child: i < children.length ? children[i] : const SizedBox.shrink(),
          ),
      ],
    );
  }
}
