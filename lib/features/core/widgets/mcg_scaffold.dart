import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mc_gallery/features/core/data/constants/const_colors.dart';
import 'package:mc_gallery/features/core/services/logging_service.dart';
import 'package:mc_gallery/l10n/generated/l10n.dart';

import '/features/core/services/connections_service.dart';
import '/features/core/services/overlay_service.dart';

class McgScaffold extends StatelessWidget with LoggingService {
  McgScaffold({
    this.appBar,
    this.bodyBuilderWaiter,
    this.body,
    this.waitingWidget,
    this.forceInternetCheck = false,
    super.key,
  });

  final AppBar? appBar;

  /// Awaits an external signal (complete) before building the body.
  final ValueListenable<bool>? bodyBuilderWaiter;
  final Widget? body;

  /// Custom widget to be used while awaiting [bodyBuilderWaiter].
  ///
  /// Defaults to using [PlatformCircularProgressIndicator].
  final Widget? waitingWidget;

  /// Enabling listing to [ConnectionState], showing a small text at the top, when connectivity is lost.
  final bool forceInternetCheck;
  final ConnectionsService _connectionsService = ConnectionsService.locate;
  final OverlayService _overlayService = OverlayService.locate;

  @override
  Widget build(BuildContext context) {
    if (forceInternetCheck) {
      _connectionsService.internetConnectionStatusListenable.addListener(
        () => _handleOverlayDisplay(context: context),
      );
      if (_connectionsService.internetConnectionStatusListenable.value ==
          InternetConnectionStatus.disconnected) _handleOverlayDisplay(context: context);
    }

    return Scaffold(
      appBar: appBar,
      body: bodyBuilderWaiter != null
          ? ValueListenableBuilder<bool>(
              valueListenable: bodyBuilderWaiter!,
              builder: (context, final isReady, child) => !isReady
                  ? Center(child: waitingWidget ?? const CircularProgressIndicator())
                  : body ?? const SizedBox.shrink(),
            )
          : body ?? const SizedBox.shrink(),
    );
  }

  void _handleOverlayDisplay({required BuildContext context}) {
    switch (_connectionsService.internetConnectionStatusListenable.value) {
      case InternetConnectionStatus.disconnected:
        _overlayService.insertOverlayEntry(
          context,
          tag: runtimeType.toString(),
          overlayEntry: OverlayEntry(
            opaque: false,
            builder: (_) => Align(
              alignment: Alignment.topCenter,
              child: Card(
                elevation: 16,
                surfaceTintColor: ConstColours.transparent,
                color: ConstColours.transparent,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    Strings.current.noInternetMessage,
                  ),
                ),
              ),
            ),
          ),
        );
        break;
      case InternetConnectionStatus.connected:
        _overlayService.removeOverlayEntry(tag: runtimeType.toString());
    }
  }
}
