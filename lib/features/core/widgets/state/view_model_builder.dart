import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../../abstracts/base_view_model.dart';

class ViewModelBuilder<T extends BaseViewModel> extends StatefulWidget {
  const ViewModelBuilder({
    required Widget Function(BuildContext context, T model) builder,
    required T Function() viewModelBuilder,
    dynamic Function()? argumentBuilder,
    super.key,
  })  : _builder = builder,
        _viewModelBuilder = viewModelBuilder,
        _argumentBuilder = argumentBuilder;

  final Widget Function(BuildContext context, T model) _builder;
  final T Function() _viewModelBuilder;
  final dynamic Function()? _argumentBuilder;

  @override
  _ViewModelBuilderState<T> createState() => _ViewModelBuilderState<T>();
}

class _ViewModelBuilderState<T extends BaseViewModel> extends State<ViewModelBuilder<T>> {
  late final T _viewModel;

  @override
  void initState() {
    _viewModel = widget._viewModelBuilder();
    _viewModel.initialise(() => mounted, widget._argumentBuilder?.call());
    super.initState();
  }

  @override
  void dispose() {
    _viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext _) => ChangeNotifierProvider.value(
        value: _viewModel,
        child: Consumer<T>(
          builder: (context, model, _) => widget._builder(context, model),
        ),
      );
}
