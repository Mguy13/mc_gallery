import 'package:envied/envied.dart';

part 'env.g.dart';

@Envied()
abstract class Env {
  @EnviedField(
    varName: 'UNSPLASH_API_KEY',
    defaultValue: '',
  )
  static const unsplashApiKey = _Env.unsplashApiKey;
}
