import 'package:flutter/material.dart';

import 'features/core/abstracts/app_setup.dart';
import 'features/core/abstracts/router/app_router.dart';
import 'features/core/data/constants/const_colors.dart';
import 'l10n/generated/l10n.dart';

class McgApp extends StatelessWidget {
  const McgApp({super.key});

  @override
  Widget build(BuildContext context) {
    final appRouter = McgRouter.locate.router;
    return MaterialApp.router(
      title: Strings.current.appTitle,
      theme: ConstThemes.materialLightTheme,
      darkTheme: ConstThemes.materialDarkTheme,
      supportedLocales: AppSetup.supportedLocales,
      routeInformationParser: appRouter.routeInformationParser,
      routerDelegate: appRouter.routerDelegate,
      routeInformationProvider: appRouter.routeInformationProvider,
    );
  }
}
