import 'dart:async';

import 'package:flutter/widgets.dart';

import 'app.dart';
import 'features/core/abstracts/app_setup.dart';

Future<void> main() async {
  await runZonedGuarded(
    () async {
      await AppSetup.initialise();
      runApp(const McgApp());
    },
    AppSetup.onUncaughtException,
  );
}
