// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(imageNumber, imageSide) =>
      "Image ${imageNumber}: size=${imageSide}";

  static String m1(searchStr, imageNumber) =>
      "Search term \'${searchStr}\' result: Image ${imageNumber}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "appTitle": MessageLookupByLibrary.simpleMessage("MC Gallery App"),
        "errorPageMessage": MessageLookupByLibrary.simpleMessage(
            "Oopsie, there has been an error. Hang tight till we do something about it."),
        "gallery": MessageLookupByLibrary.simpleMessage("Gallery"),
        "imageCarousel": MessageLookupByLibrary.simpleMessage("Image carousel"),
        "imageDetails": MessageLookupByLibrary.simpleMessage(
            "Lorem ipsum dolor sit amet. A odio aliquam est sunt explicabo cum galisum asperiores qui voluptas tempora qui aliquid similique. Ut quam laborum ex nostrum recusandae ab sunt ratione quo tempore corporis 33 voluptas nulla aut obcaecati perspiciatis.\n\nAd eveniet exercitationem ad odit quidem aut omnis corporis ea nulla illum qui quisquam temporibus? Est obcaecati similique et quisquam unde ea impedit mollitia ea accusamus natus hic doloribus quis! Et dolorem rerum id doloribus sint ea porro quia ut reprehenderit ratione?"),
        "imageNameFetch": m0,
        "imageNameSearch": m1,
        "local": MessageLookupByLibrary.simpleMessage("Local"),
        "noInternetMessage": MessageLookupByLibrary.simpleMessage(
            "Are you sure that you\'re connected to the internet?"),
        "searchForImage":
            MessageLookupByLibrary.simpleMessage("Search for your image"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Something went wrong"),
        "startLoadingPrompt":
            MessageLookupByLibrary.simpleMessage("Press me to start loading"),
        "web": MessageLookupByLibrary.simpleMessage("Web")
      };
}
