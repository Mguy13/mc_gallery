# mc_gallery

## Dart docs explanation
The first line of docs are like normal Dart docs. The second line is less of a 'further'
 explanation, and more of an active commentary for the viewer.

## Emulation
### Main API
The unsplash_images_api contains code for fetching data from an external website. Since I have used 'source.unsplash.com', there is no actual daa
data (in the form of protected URLs or raw JSON like Facebook API) that is sent back. So this base URL can directly be used to cache the images.<br>

But I wanted to give a more realistic procedure. So, I manually serialize the same data about the URL to JSON and then deserialize it, just a step
further on needlessly. In addition to it, constant fetch delays have added in as well. But in theory, the first serialization can be replaced by actual data-fetch,
while the rest works as expected

### Search API
Since websites also have a server-side search call that is available, functionality had been included to make use of that too. But again, this is mocked
by the steps mentioned above.

## Maintaining scope / limitations
Since it's just a demo assignment, I have tried to keep the scope of the functionalities small. By that, I mean I have not fully optimized performance-critical sections,
or fixed some rare-case bugs. But instead I have left either a 'todo' for them, or mentioned that in the docs themselves
### Native look
I did not adhere to any native-look principles for the device platform, for this assignment <br>
In my other projects, I had discovered the [PlatformWidget](https://pub.dev/packages/flutter_platform_widgets) plugin,
and had started using them to follow a proper Material and Apple's Human Interface Guidelines. 

## Extra quirks
A lot of the empty directories and unused asset files are left in, as I used my project templated from my other templates. I decided to leave them in, in any case since they
may give a better indication about how I code, should you choose to look into them. A lot of the functionality that I used, is something that I have already made for my previous project(s),
so it was not as much work as it seems. One of my recent ones, (luckily) literally had a Gallery feature as well, for congregating social media posts into one place. So a lot of assets were reused from there.

Some of the extra quirks that I added in are:
1. Dark mode support.
2. Live search -> both for locally (cached) image files, and Web (using a website's search API).
3. Clearing image cache on app going to background, and then restarting the program (way easier on an emulator using hot restart).
   + I found it easier to directly use the change in app life cycle, than implement a pull-down-refresh.
4. Logging.
5. Local storage of favourites state -> on restart, the favourites stay.
   + Not very useful on a real device as app restart can only be triggered with app life cycle state change, which would clear the download cache,
   which would be an invalidate them -> so clearing them away with the image cache for now.
6. Using an environment key manager for storing API keys

## Documentation
### Class UML
[<img height="300" src="/Users/mehulahal/StudioProjects/mc_gallery/doc/Class UML.svg" title="Class UML"/>](doc/Class%20UML.svg)

### API Reference
[Dart docs](doc/api/index.html)
